package com.agero.nccsdk.internal.data.network.aws.kinesis;

/**
 * Created by james hermida on 11/13/17.
 */


public interface KinesisClient {

    void save(String data, String streamName);
    void submit();
    long getDiskBytesUsed();

}
