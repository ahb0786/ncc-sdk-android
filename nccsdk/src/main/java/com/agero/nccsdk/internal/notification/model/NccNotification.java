package com.agero.nccsdk.internal.notification.model;

import android.support.annotation.DrawableRes;

/**
 * Created by james hermida on 12/8/17.
 */

public class NccNotification {

    private int id;
    private int drawableId;
    private String contentTitle;
    private String contentText;

    public NccNotification(int id, @DrawableRes int drawableId, String contentTitle, String contentText) {
        this.id = id;
        this.drawableId = drawableId;
        this.contentTitle = contentTitle;
        this.contentText = contentText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getContentText() {
        return contentText;
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }
}
