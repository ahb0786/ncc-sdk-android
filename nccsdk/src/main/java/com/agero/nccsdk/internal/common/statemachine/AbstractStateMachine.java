package com.agero.nccsdk.internal.common.statemachine;

/**
 * Created by james hermida on 11/6/17.
 */

public abstract class AbstractStateMachine<T extends State> implements StateMachine<T> {

    protected T currentState;

    protected AbstractStateMachine(T currentState) {
        this.currentState = currentState;
    }

    @Override
    public void setState(T newState) {
        currentState.onExit();
        this.currentState = newState;
        currentState.onEnter();
    }
}
