package com.agero.nccsdk.internal.common.statemachine;

import com.agero.nccsdk.domain.config.NccConfig;

/**
 * Created by james hermida on 11/14/17.
 */

public interface SdkConfig extends NccConfig {
}
