package com.agero.nccsdk.internal.data.network.aws.kinesis;

import com.agero.nccsdk.internal.auth.AuthManager;
import com.agero.nccsdk.internal.log.Timber;
import com.amazonaws.auth.AWSCredentialsProvider;

import java.io.File;

import javax.inject.Inject;

/**
 * Created by james hermida on 10/10/17.
 */

public class LogKinesisClient extends AbstractKinesisClient {

    @Inject
    public LogKinesisClient(File directory, AWSCredentialsProvider credentialsProvider, AuthManager authManager) {
        super(directory, credentialsProvider, authManager);
    }

    @Override
    public void submit() {
        Timber.v("Submitting logs to stream. Bytes used: " + getDiskBytesUsed());
        super.submit();
    }
}
