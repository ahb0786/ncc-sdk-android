package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccPhoneCallsData;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.log.Timber;

import static android.telephony.TelephonyManager.CALL_STATE_IDLE;
import static android.telephony.TelephonyManager.CALL_STATE_OFFHOOK;
import static android.telephony.TelephonyManager.CALL_STATE_RINGING;

public class NccPhoneCalls extends NccAbstractBroadcastReceiverSensor {

    // Maintain static variables to remember data between instantiations of this receiver
    public static int lastState = CALL_STATE_IDLE;
    private static boolean isIncoming;
    private static boolean incomingCallStatePreviouslyIgnored = false;
    
    public NccPhoneCalls(Context context, NccConfig config) {
        super(context, NccPhoneCalls.class.getSimpleName(), NccSensorType.PHONE_CALLS, config);
    }

    @Override
    String[] getActions() {
        return new String[]{
                Intent.ACTION_NEW_OUTGOING_CALL,
                TelephonyManager.ACTION_PHONE_STATE_CHANGED
        };
    }

    @Override
    boolean isLocalReceiver() {
        return false;
    }

    @Override
    public void onDataReceived(Context context, Intent intent) {
        // This receiver is listening to two intent filters:
        //     (1) android.intent.action.NEW_OUTGOING_CALL
        //     (2) android.intent.action.PHONE_STATE
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_NEW_OUTGOING_CALL)) {
            // do nothing
        } else {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                String stateString = extras.getString(TelephonyManager.EXTRA_STATE);
                if (StringUtils.isNullOrEmpty(stateString)) {
                    Timber.w("Intent did not include EXTRA_STATE extra.");
                    return;
                }

                int currentState = 0;
                if (stateString.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    currentState = CALL_STATE_IDLE;
                } else if (stateString.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    currentState = CALL_STATE_OFFHOOK;
                } else if (stateString.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    currentState = CALL_STATE_RINGING;
                }

                if (currentState != lastState) {
                    Timber.v("Call state transition: %s -> %s", getStateString(lastState), stateString);
                }

                onCallStateChanged(context, currentState);
            } else {
                Timber.w("Intent extras was null");
            }
        }
    }

    // Handle actual events
    // Incoming call - goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
    // Outgoing call - goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
    public void onCallStateChanged(Context context, int currentState) {
        if (lastState == currentState) {
            if (incomingCallStatePreviouslyIgnored) {
                // OFFHOOK state for incoming call will be ignored since the state when
                // a call is in progress is OFFHOOK.
                Timber.w("Call in progress. Incoming call state being ignored: %s", getStateString(currentState));
                incomingCallStatePreviouslyIgnored = false;
            } else {
                // Duplicate events broadcasts for Lollipop devices
                Timber.w("Ignoring duplicate state received: %s", getStateString(currentState));
            }
            return;
        }
        switch (currentState) {
            case CALL_STATE_RINGING:
                if (lastState == CALL_STATE_OFFHOOK) {
                    // Transition of OFFHOOK -> RINGING means a call is already in progress
                    // and there's a new incoming call on the other line.
                    // For each incoming call while a call is already in progress, there will
                    // be an incoming call state flow, but no disconnected call state flow.
                    // Backend is expecting an equal amount of connected and disconnected events
                    // based on how many calls are made during a trip.
                    // In this case we want to ignore the states for additional incoming calls states,
                    // RINGING -> OFFHOOK, while on another call. This condition will take care of the RINGING state,
                    // and the OFFHOOK state will be seen as a duplicate, and therefore ignored.
                    Timber.w("Call in progress. Incoming call state being ignored: %s", getStateString(currentState));
                    incomingCallStatePreviouslyIgnored = true;
                    return;
                } else {
                    isIncoming = true;
                    onIncomingCallReceived(context);
                }
                break;
            case CALL_STATE_OFFHOOK:
                // Transition of RINGING -> OFFHOOK are pickups of incoming calls.  Nothing done on them
                if (lastState != CALL_STATE_RINGING) {
                    isIncoming = false;
                    onOutgoingCallStarted(context);
                } else {
                    isIncoming = true;
                    onIncomingCallAnswered(context);
                }

                break;
            case CALL_STATE_IDLE:
                // Transition to IDLE - end of a call.  What type depends on previous state(s)
                if (lastState == CALL_STATE_RINGING) {
                    // Ring but no pickup - missed call
                    onMissedCall(context);
                } else if (isIncoming) {
                    onIncomingCallEnded(context);
                } else {
                    onOutgoingCallEnded(context);
                }
                break;
        }
        lastState = currentState;
    }

    private String getStateString(int state) {
        switch (state) {
            case CALL_STATE_IDLE:
                return TelephonyManager.EXTRA_STATE_IDLE;
            case CALL_STATE_RINGING:
                return TelephonyManager.EXTRA_STATE_RINGING;
            case CALL_STATE_OFFHOOK:
                return TelephonyManager.EXTRA_STATE_OFFHOOK;
            default:
                return "UNKNOWN";
        }
    }

    private void onIncomingCallReceived(Context context) {
        postData(NccPhoneCallsData.State.INCOMING_CALL_RECEIVED);
    }

    private void onIncomingCallAnswered(Context context) {
        postData(NccPhoneCallsData.State.INCOMING_CALL_ANSWERED);
    }

    private void onIncomingCallEnded(Context context) {
        postData(NccPhoneCallsData.State.INCOMING_CALL_ENDED);
    }

    private void onOutgoingCallStarted(Context context) {
        postData(NccPhoneCallsData.State.OUTGOING_CALL_STARTED);
    }

    private void onOutgoingCallEnded(Context context) {
        postData(NccPhoneCallsData.State.OUTGOING_CALL_ENDED);
    }

    private void onMissedCall(Context context) {
        postData(NccPhoneCallsData.State.MISSED_CALL);
    }

    private void postData(NccPhoneCallsData.State state) {
        super.postData(
                new NccPhoneCallsData(
                        System.currentTimeMillis(),
                        state
                )
        );
    }
}
