package com.agero.nccsdk.domain.sensor;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.adt.detection.domain.motion.ActivityRecognitionIntentService;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.config.NccMotionConfig;
import com.agero.nccsdk.domain.data.NccAbstractSensorData;
import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.tasks.OnCompleteListener;

import java.util.List;

/**
 * Created by james hermida on 8/16/17.
 */

public class NccMotion extends NccAbstractBroadcastReceiverSensor {

    private PendingIntent activityDetectionPendingIntent;

    private final ActivityRecognitionClient activityRecognitionClient;
    private final OnCompleteListener<Void> completeListener;

    /**
     * Creates an instance of NccLocation
     *
     * @param context The Context allowing access to application-specific resources and classes, as well as
     * up-calls for application-level operations such as launching activities,
     * broadcasting and receiving intents, etc.
     */
    public NccMotion(Context context, NccConfig config) {
        super(context, NccMotion.class.getSimpleName(), NccSensorType.MOTION_ACTIVITY, config);
        activityRecognitionClient = ActivityRecognition.getClient(context);
        completeListener = task -> {
            if (task.isSuccessful()) {
                isStreaming = true;
                Timber.v("Request succeeded");
            } else {
                Timber.v("Request failed");
            }
        };
    }

    @Override
    String[] getActions() {
        return new String[]{
                ActivityRecognitionIntentService.ACTION_ACTIVITY_UPDATE
        };
    }

    @Override
    boolean isLocalReceiver() {
        return true;
    }

    @Override
    public void onDataReceived(Context context, Intent intent) {
        ActivityRecognitionResult result = intent.getParcelableExtra(ActivityRecognitionIntentService.ACTIVITY_RESULT);
        List<DetectedActivity> activities = result.getProbableActivities();
        for (DetectedActivity activity : activities) {
            NccAbstractSensorData data = new NccMotionData(result.getTime(), activity.getType(), activity.getConfidence());
            postData(data);
        }
    }

    /**
     * See {@link NccSensor#startStreaming()}
     */
    @Override
    public void startStreaming() {
        super.startStreaming();
        requestActivityUpdates();
    }

    /**
     * See {@link NccSensor#stopStreaming()}
     */
    @Override
    public void stopStreaming() {
        super.stopStreaming();
        removeActivityUpdates();
    }

    /**
     * Requests motion activity updates from ActivityRecognitionApi
     */
    @SuppressWarnings({"MissingPermission"})
    private void requestActivityUpdates() {
        NccMotionConfig config = (NccMotionConfig) this.config;
        Timber.v("Requesting activity motion interval (ms): %d", config.getInterval());
        activityRecognitionClient.requestActivityUpdates(config.getInterval(), getActivityDetectionPendingIntent())
                .addOnCompleteListener(completeListener);
    }

    /**
     * Gets a PendingIntent to be invoked for motion activity updates
     *
     * @return PendingIntent to be invoked for motion activity updates
     */
    private PendingIntent getActivityDetectionPendingIntent() {
        if (activityDetectionPendingIntent == null) {
            Intent intent = new Intent(applicationContext, ActivityRecognitionIntentService.class);

            // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
            // requestActivityUpdates() and removeActivityUpdates().
            activityDetectionPendingIntent = PendingIntent.getService(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        return activityDetectionPendingIntent;
    }

    /**
     * Removes motion activity updates from ActivityRecognitionApi
     */
    @SuppressWarnings({"MissingPermission"})
    private void removeActivityUpdates() {
        Timber.v("Removing activity updates");
        activityRecognitionClient.removeActivityUpdates(getActivityDetectionPendingIntent())
                .addOnCompleteListener(completeListener);

        if (activityDetectionPendingIntent != null) {
            activityDetectionPendingIntent.cancel();
            activityDetectionPendingIntent = null;
        }
    }
}
