package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.content.Intent;

/**
 * Interface definition used with {@link NccBroadcastReceiver} which mirrors
 * {@link android.content.BroadcastReceiver#onReceive(Context, Intent)}. This
 * allows subclasses of {@link NccAbstractBroadcastReceiverSensor} to process
 * data similarly to a normal Broadcast Receiver.
 */
public interface NccBroadcastReceiverDelegate {

    void onDataReceived(Context context, Intent intent);

}
