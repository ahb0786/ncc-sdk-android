package com.agero.nccsdk.adt.detection.end.algo;

import android.content.Context;
import android.os.Build;

import com.agero.nccsdk.domain.data.NccLocationSettingsData;
import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 11/14/17.
 */

public class LocationModeDrivingEndStrategy extends DrivingEndStrategy<NccLocationSettingsData> {

    public LocationModeDrivingEndStrategy(Context context) {
        super(context);
    }

    @Override
    public void evaluate(NccLocationSettingsData data) {
        if (data == null) {
            Timber.e("Unable to evaluate location settings. data is null");
        } else if (isKitKatOrAbove()) {
            Timber.d("Location mode updated: %s", data.getMode().getName());
            if (data.getMode() != NccLocationSettingsData.Mode.HIGH_ACCURACY) {
                sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
            }
        } else {
            Timber.d("Location mode updated: NETWORK(%s), GPS(%s)", data.getProvider().isNetworkEnabled(), data.getProvider().isGpsEnabled());
            if (!data.getProvider().isGpsEnabled() || !data.getProvider().isNetworkEnabled()) {
                sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
            }
        }
    }

    @Override
    public void releaseResources() {

    }

    private boolean isKitKatOrAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }
}
