package com.agero.nccsdk.adt.detection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.agero.nccsdk.internal.log.Timber;

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Timber.d("Device boot complete");
    }
}
