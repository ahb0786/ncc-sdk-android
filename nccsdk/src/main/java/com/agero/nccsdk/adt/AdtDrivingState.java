package com.agero.nccsdk.adt;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.adt.detection.end.AdtEndMonitor;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.ubi.collection.SensorCollector;

import static com.agero.nccsdk.adt.event.AdtEventHandler.ACTION_SESSION_END;
import static com.agero.nccsdk.adt.event.AdtEventHandler.ACTION_SESSION_START;

public class AdtDrivingState extends AdtAbstractState {

    private SensorCollector endMonitor;

    AdtDrivingState(Context context) {
        super(context);
    }

    @Override
    public void onEnter() {
        Timber.d("Entered AdtDrivingState");
        sendSessionStartBroadcast();

        endMonitor = new AdtEndMonitor(
                applicationContext,
                new NccSensorType[]{
                        NccSensorType.LOCATION,
                        NccSensorType.MOTION_ACTIVITY,
                        NccSensorType.LOCATION_MODE
                }
        );

        endMonitor.start();
    }

    @Override
    public void onExit() {
        Timber.d("Exited AdtDrivingState");
        sendSessionEndBroadcast();

        endMonitor.stop();
    }

    @Override
    public void startMonitoring(StateMachine<AdtState> machine) {
        // DO NOTHING
    }

    @Override
    public void stopMonitoring(StateMachine<AdtState> machine) {
        // DO NOTHING
    }

    @Override
    public void startDriving(StateMachine<AdtState> machine) {
        // DO NOTHING
    }

    @Override
    public void stopDriving(StateMachine<AdtState> machine) {
        machine.setState(new AdtMonitoringState(applicationContext));
    }

    private void sendSessionStartBroadcast() {
        Intent i = new Intent(ACTION_SESSION_START);
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(i);
        Timber.d("Broadcast sent: %s", i.getAction());
    }

    private void sendSessionEndBroadcast() {
        Intent i = new Intent(ACTION_SESSION_END);
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(i);
        Timber.d("Broadcast sent: %s", i.getAction());
    }
}
