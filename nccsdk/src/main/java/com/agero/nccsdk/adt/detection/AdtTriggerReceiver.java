package com.agero.nccsdk.adt.detection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.internal.log.Timber;

import static com.agero.nccsdk.adt.AdtStateMachine.TRIGGER_TYPE;

/**
 * Created by james hermida on 9/21/17.
 */

public class AdtTriggerReceiver extends BroadcastReceiver {

    private final OnTriggerReceived listener;

    public AdtTriggerReceiver(OnTriggerReceived listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(TRIGGER_TYPE)) {
            AdtTriggerType triggerType = (AdtTriggerType) intent.getSerializableExtra(TRIGGER_TYPE);
            if (listener == null) {
                Timber.w("%s.onReceive() - listener is null", AdtTriggerReceiver.class.getSimpleName());
            } else {
                listener.onTriggerReceived(triggerType);
            }
        } else {
            Timber.w("Broadcast received with no TRIGGER_TYPE extra");
        }
    }

    public interface OnTriggerReceived {
        void onTriggerReceived(final AdtTriggerType adtTriggerType);
    }
}
