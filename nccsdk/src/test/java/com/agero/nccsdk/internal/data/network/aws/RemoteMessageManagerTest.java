package com.agero.nccsdk.internal.data.network.aws;

import com.agero.nccsdk.internal.data.network.aws.iot.IotClient;
import com.agero.nccsdk.internal.data.preferences.NccSharedPrefs;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Constructor;

import static org.mockito.Mockito.verify;

/**
 * Created by james hermida on 10/19/17.
 */

@RunWith(PowerMockRunner.class)
public class RemoteMessageManagerTest {

    private RemoteMessageManager remoteMessageManager;

    @Mock
    private IotClient mockIotClient;

    @Mock
    private NccSharedPrefs mockSharedPrefs;

    @Before
    public void setUp() throws Exception {
        Constructor[] ctors = RemoteMessageManager.class.getDeclaredConstructors();
        remoteMessageManager = (RemoteMessageManager) ctors[0].newInstance(mockIotClient, mockSharedPrefs);
    }

    @Test
    public void onConnected() throws Exception {
        String testUserId = "testUserId";
        PowerMockito.when(mockSharedPrefs, "getUserId").thenReturn(testUserId);

        remoteMessageManager.onConnected();
        verify(mockIotClient).subscribeToTopic(testUserId, remoteMessageManager);

        // FIXME mIotClient.addOnConnectedCallback(this) interaction occurs after .onConnected()
//        PowerMockito.when(mockSharedPrefs, "getUserId").thenReturn("");
//        remoteMessageManager.onConnected();
//        verifyNoMoreInteractions(mockIotClient);
    }

    // TODO test for onMessageArrived() when message handling is implemented
}
