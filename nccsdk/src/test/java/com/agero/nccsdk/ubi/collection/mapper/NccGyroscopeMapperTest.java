package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccGyroscopeData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccGyroscopeMapperTest {

    private AbstractSensorDataMapper mapper = new NccGyroscopeMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        float rX = 1.2345f;
        float rY = 2.3456f;
        float rZ = 3.4567f;
        NccSensorData data = new NccGyroscopeData(timeUtc, rX, rY, rZ);

        String expected = "R," + rX + "," + rY + "," + rZ + ",NA,NA,NA,NA,NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
