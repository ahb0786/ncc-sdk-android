package com.agero.nccsdk.adt.detection.end.algo;

import android.content.Context;

import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.adt.AdtTriggerType;
import com.google.android.gms.location.DetectedActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by james hermida on 11/29/17.
 */

@RunWith(PowerMockRunner.class)
public class MotionDrivingEndStrategyTest {

    @Mock
    private Context mockContext;

    private int testThreshold = 25;
    private MotionDrivingEndStrategy motionCollectionEndStrategy;

    @Before
    public void setUp() throws Exception {
        motionCollectionEndStrategy = PowerMockito.spy(new MotionDrivingEndStrategy(mockContext, testThreshold));
    }

    @Test
    public void evaluate_null_data() throws Exception {
        motionCollectionEndStrategy.evaluate(null);

        verify(motionCollectionEndStrategy, times(0)).sendCollectionEndBroadcast(any(AdtTriggerType.class));
    }

    @Test
    public void evaluate_ends_collection_confidence_matches_threshold() throws Exception {
        PowerMockito.doNothing().when(motionCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));

        NccMotionData data = new NccMotionData(0, DetectedActivity.WALKING, testThreshold);
        motionCollectionEndStrategy.evaluate(data);
        verify(motionCollectionEndStrategy).sendCollectionEndBroadcast(AdtTriggerType.STOP_MOTION);
    }

    @Test
    public void evaluate_ends_collection_confidence_above_threshold() throws Exception {
        PowerMockito.doNothing().when(motionCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));

        NccMotionData data = new NccMotionData(0, DetectedActivity.WALKING, testThreshold + 10);
        motionCollectionEndStrategy.evaluate(data);

        verify(motionCollectionEndStrategy).sendCollectionEndBroadcast(AdtTriggerType.STOP_MOTION);
    }

    @Test
    public void evaluate_continues_collection_invalid_activity() throws Exception {
        PowerMockito.doNothing().when(motionCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));

        NccMotionData data = new NccMotionData(0, DetectedActivity.RUNNING, testThreshold);
        motionCollectionEndStrategy.evaluate(data);

        verify(motionCollectionEndStrategy, times(0)).sendCollectionEndBroadcast(AdtTriggerType.STOP_MOTION);
    }

    @Test
    public void evaluate_continues_collection_invalid_confidence() throws Exception {
        PowerMockito.doNothing().when(motionCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));

        NccMotionData data = new NccMotionData(0, DetectedActivity.WALKING, testThreshold - 10);
        motionCollectionEndStrategy.evaluate(data);

        verify(motionCollectionEndStrategy, times(0)).sendCollectionEndBroadcast(AdtTriggerType.STOP_MOTION);
    }
}
