package com.agero.nccsdk.lbt;

import com.agero.nccsdk.NccSensorListener;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Created by james hermida on 11/7/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class LbtStateMachineTest {

    @Mock
    LbtInactiveState mockInactiveState;

    @Mock
    NccSensorListener mockSensorListener;

    private LbtStateMachine lbtStateMachine;

    @Before
    public void setUp() throws Exception {
        lbtStateMachine = new LbtStateMachine(mockInactiveState);
    }

    @Test
    public void startTracking() throws Exception {
        lbtStateMachine.addTrackingListener(mockSensorListener);
        verify(mockInactiveState).addTrackingListener(lbtStateMachine, mockSensorListener);
    }

    @Test
    public void stopTracking() throws Exception {
        lbtStateMachine.removeTrackingListener(mockSensorListener);
        verify(mockInactiveState).removeTrackingListener(lbtStateMachine, mockSensorListener);
    }
}
