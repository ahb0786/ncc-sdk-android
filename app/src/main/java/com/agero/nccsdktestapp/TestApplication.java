package com.agero.nccsdktestapp;

import android.app.Application;
import android.util.Log;

import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSdkInterface;

/**
 * Created by james hermida on 8/28/17.
 */

public class TestApplication extends Application {

    private final String TAG = TestApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        final NccSdkInterface nccSdkInterface = NccSdk.getNccSdk(this);
        nccSdkInterface.authenticate(BuildConfig.SDK_API_KEY, new NccSdkInterface.AuthenticateCallback() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "AuthenticateCallback.onSuccess()");
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.e(TAG, throwable.getMessage(), throwable);
            }
        });
    }
}
